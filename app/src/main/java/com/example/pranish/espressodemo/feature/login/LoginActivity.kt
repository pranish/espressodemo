package com.example.pranish.espressodemo.feature.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.pranish.espressodemo.R
import com.example.pranish.espressodemo.feature.home.HomeActivity
import com.example.pranish.espressodemo.model.Login
import kotlinx.android.synthetic.main.activity_main.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        btnSignIn?.setOnClickListener(this)
        btnSignUp?.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            btnSignIn -> doLoginCall(Login(edtUsername?.text.toString().trim(), edtPassword?.text.toString().trim()))
        }
    }

    private fun doLoginCall(login: Login) {
        when {
            login.userName.isEmpty() -> showToast(R.string.errorEmptyUsername)
            login.password.isEmpty() -> showToast(R.string.errorEmptyPassword)
            else -> navigateToHomePage()
        }

    }


    private fun showToast(id: Int) {
        Toast.makeText(this, getString(id), Toast.LENGTH_SHORT).show()
    }

    private fun navigateToHomePage() {
        startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
    }
}
