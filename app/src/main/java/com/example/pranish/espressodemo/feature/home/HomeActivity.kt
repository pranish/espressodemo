package com.example.pranish.espressodemo.feature.home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.pranish.espressodemo.R

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}
